package app.android.media.model

data class ModelAudio(
    val id : String,
    val title : String,
    val artist : String,
    val duration : String,
    val album : String,
    val data : String,
    val size : String,
    val albumID : String
)
