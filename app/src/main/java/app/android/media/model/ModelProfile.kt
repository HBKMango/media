package app.android.media.model

data class ModelProfile(
    val name : String,
    val surname : String,
    val description : String
)
