package app.android.media.ui.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import app.android.media.R
import app.android.media.databinding.FragmentProfileBinding
import app.android.media.model.ModelProfile
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File
import java.lang.Exception

class ProfileFragment : Fragment() {

    lateinit var bind : FragmentProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        bind = FragmentProfileBinding.inflate(inflater, container, false)
        return bind.root
    }

    override fun onStart() {
        super.onStart()

        getData()

        bind.imgProfile.setOnClickListener {
            selectImage()
        }

        bind.save.setOnClickListener {
            if (bind.name.text.isNotEmpty() && bind.surname.text.isNotEmpty() && bind.description.text.isNotEmpty()){
                val profile = ModelProfile(
                    name = bind.name.text.toString(),
                    surname = bind.surname.text.toString(),
                    description = bind.description.text.toString()
                )

                saveData(profile)

                Toast.makeText(context, "Data saved", Toast.LENGTH_SHORT).show()
            }
            else{
                Toast.makeText(context, "Please complete data", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun selectImage() {
        ImagePicker.with(this)
            .crop()	    			//Crop image(Optional), Check Customization for more option
            .compress(1024)			//Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            Activity.RESULT_OK -> {

                val fileUri = data?.data

                bind.imgProfile.setImageURI(fileUri)

                val pref = context!!.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
                pref.putString("image", fileUri!!.path)
                pref.apply()


            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun saveData(e: ModelProfile){
        val pref = context!!.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE).edit()
        val jsonString = Gson().toJson(e)
        pref.putString("data", jsonString).apply()
    }

    @SuppressLint("CommitPrefEdits")
    private fun getData(){

        val pref = context!!.getSharedPreferences(getString(R.string.prefs_file), Context.MODE_PRIVATE)

        val e = pref.getString("data", null)

        if (e != null){
            val a = Gson().fromJson<ModelProfile>(e, object : TypeToken<ModelProfile>() {}.type)

            if (a.name.isNotEmpty()){

                bind.name.setText(a.name)
                bind.surname.setText(a.surname)
                bind.description.setText(a.description)
            }

        }

        val image = pref.getString("image", null)

        if (image != null) {
            try {
                bind.imgProfile.setImageURI(Uri.fromFile(File(image)))
            } catch (e : Exception){
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
            }
        }
    }
}