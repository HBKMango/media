package app.android.media.ui.activity

import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import app.android.media.adapter.AdapterSingleImage
import app.android.media.data.DataSource
import app.android.media.databinding.ActivityImageBinding
import com.bumptech.glide.Glide
import com.stfalcon.imageviewer.StfalconImageViewer

class Image : AppCompatActivity() {

    lateinit var binding : ActivityImageBinding
    private lateinit var adapter: AdapterSingleImage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityImageBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onStart() {
        super.onStart()

        StfalconImageViewer.Builder(this, DataSource.loadImages(this@Image)) { view, image ->
            Glide.with(this).load(image).into(view)
        }.show()

        /*ImageViewer.Builder(this, DataSource.loadImages(this@Image))
            .setStartPosition(0)
            .show()*/

        /*adapter = AdapterSingleImage()
        binding.recycler.layoutManager = LinearLayoutManager(
            this@Image,
            RecyclerView.HORIZONTAL,
            false
        )
        binding.recycler.adapter = adapter

        Dexter.withContext(this@Image)
            .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                @RequiresApi(Build.VERSION_CODES.Q)
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    adapter.swapData(DataSource.loadImages(this@Image))
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {
                    p1!!.continuePermissionRequest()
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {}
            }).check()*/
    }
}