package app.android.media.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import app.android.media.R
import app.android.media.adapter.PageAdapter
import app.android.media.databinding.ActivityMainBinding
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {

    private lateinit var mBinding : ActivityMainBinding
    private lateinit var adapter : PageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityMainBinding.inflate(layoutInflater)
        val view = mBinding.root

        setContentView(view)

        adapter = PageAdapter(supportFragmentManager)

        mBinding.tab.tabGravity = TabLayout.GRAVITY_FILL
        mBinding.viewPager.adapter = adapter
        mBinding.tab.setupWithViewPager(mBinding.viewPager)
        mBinding.tab.getTabAt(0)!!.icon = ContextCompat.getDrawable(applicationContext,
            R.drawable.ic_image
        )
        mBinding.tab.getTabAt(1)!!.icon = ContextCompat.getDrawable(applicationContext,
            R.drawable.ic_music
        )
        mBinding.tab.getTabAt(2)!!.icon = ContextCompat.getDrawable(applicationContext,
            R.drawable.ic_person
        )

    }

    override fun onStart() {
        super.onStart()

    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}