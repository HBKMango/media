package app.android.media.ui.fragment

import android.Manifest
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import app.android.media.data.DataSource
import app.android.media.adapter.AdapterImage
import app.android.media.adapter.ImageListener
import app.android.media.databinding.FragmentPicturesBinding
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.stfalcon.imageviewer.StfalconImageViewer
import java.io.File

class PicturesFragment : Fragment(), ImageListener {

    private var binding: FragmentPicturesBinding? = null
    private lateinit var adapter: AdapterImage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val folder = File(context!!.getExternalFilesDir(null)!!.absolutePath + File.separator + "Pictures")
        if (!folder.exists()) {
            folder.mkdirs()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentPicturesBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onStart() {
        super.onStart()

        adapter = AdapterImage()
        adapter.listener = this
        binding!!.recycler.layoutManager = LinearLayoutManager(context)
        binding!!.recycler.adapter = adapter

        val mLayoutManager = GridLayoutManager(context, 3)
        binding!!.recycler.layoutManager = mLayoutManager

        Dexter.withContext(context!!)
            .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                @RequiresApi(Build.VERSION_CODES.Q)
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    adapter.swapData(DataSource.loadImages(context!!))
                }

                override fun onPermissionRationaleShouldBeShown(p0: PermissionRequest?, p1: PermissionToken?) {
                    p1!!.continuePermissionRequest()
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {}
            }).check()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onSelected(position: Int, image: String) {
        StfalconImageViewer.Builder(context, DataSource.loadImages(context!!)) { view, image ->
            Glide.with(this).load(image).into(view)
        }.withStartPosition(position).show()
    }
}