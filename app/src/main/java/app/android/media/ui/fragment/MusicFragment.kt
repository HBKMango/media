package app.android.media.ui.fragment

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import app.android.media.data.DataSource
import app.android.media.model.ModelAudio
import app.android.media.adapter.AdapterSongs
import app.android.media.adapter.SongListener
import app.android.media.databinding.FragmentMusicBinding

class MusicFragment : Fragment(), SongListener {

    lateinit var binding : FragmentMusicBinding
    private lateinit var adapter: AdapterSongs
    lateinit var player : PlayerFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = AdapterSongs()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMusicBinding.inflate(inflater, container, false)
        return binding.root
    }


    @RequiresApi(Build.VERSION_CODES.R)
    override fun onStart() {
        super.onStart()

        adapter.listener = this
        binding.recycler.layoutManager = LinearLayoutManager(context)
        binding.recycler.adapter = adapter

        adapter.swapData(DataSource.loadMusic(context!!))
    }

    override fun onSongSelected(position: Int, song: ModelAudio) {
        player = PlayerFragment(song)
        player.show(childFragmentManager, "Dialog")
    }
}