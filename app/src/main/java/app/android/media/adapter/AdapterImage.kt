package app.android.media.adapter

import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.android.media.databinding.ItemImageBinding
import com.bumptech.glide.Glide
import com.koushikdutta.ion.Ion

class AdapterImage : RecyclerView.Adapter<AdapterImage.ImageViewHolder>() {

    private var data: List<String> = ArrayList()
    lateinit var listener: ImageListener

    fun swapData(data: List<String>) {
        this.data = data
        notifyDataSetChanged()
    }

    class ImageViewHolder(private val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root){
        private var listener: ImageListener? = null
        private var lastClickTime: Long = 0

        fun bind(item: String, listener: ImageListener, position: Int) = with(binding) {
            this@ImageViewHolder.listener = listener

            Glide.with(binding.root.context)
                .load(item)
                .into(image)

            root.setOnClickListener {

                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return@setOnClickListener;
                }

                lastClickTime = SystemClock.elapsedRealtime()

                this@ImageViewHolder.listener!!.onSelected(position, item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = ItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int)
    = holder.bind(data[position], listener, position)
}

interface ImageListener {
    fun onSelected(position: Int, image: String)
}