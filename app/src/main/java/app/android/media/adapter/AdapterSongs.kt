package app.android.media.adapter

import android.annotation.SuppressLint
import android.os.SystemClock
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.android.media.model.ModelAudio
import app.android.media.databinding.ItemSongBinding
import com.bumptech.glide.Glide

class AdapterSongs : RecyclerView.Adapter<AdapterSongs.SongViewHolder>() {

    private var data: List<ModelAudio> = ArrayList()
    lateinit var listener: SongListener

    fun swapData(data: List<ModelAudio>) {
        this.data = data
        notifyDataSetChanged()
    }

    inner class SongViewHolder(private val binding: ItemSongBinding) : RecyclerView.ViewHolder(binding.root){

        private var listener: SongListener? = null
        private var lastClickTime: Long = 0

        fun bind(listener: SongListener, item: ModelAudio, position: Int) = with(binding) {

            this@SongViewHolder.listener = listener

            Glide.with(binding.root.context)
                .load(getAlbumImagePath(item.albumID.toLong()))
                .error("https://images.unsplash.com/photo-1505740420928-5e560c06d30e?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHw%3D&w=1000&q=80")
                .into(image)

            title.text = item.title
            autor.text = item.artist
            duration.text = item.duration

            root.setOnClickListener {

                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return@setOnClickListener;
                }
                lastClickTime = SystemClock.elapsedRealtime()

                this@SongViewHolder.listener!!.onSongSelected(position, item)
            }
        }

        @SuppressLint("Recycle")
        private fun getAlbumImagePath(albumID: Long): String? {
            val uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI
            val projection = arrayOf(MediaStore.Audio.Albums.ALBUM_ART)
            val selection = MediaStore.Audio.Albums._ID + "=?"
            val args = arrayOf(albumID.toString())

            val cursor = binding.root.context.contentResolver.query(uri, projection, selection, args, null)

            var albumPath: String? = null

            if(cursor != null) {
                if(cursor.moveToFirst()) albumPath = cursor.getString(cursor.getColumnIndex(
                    MediaStore.Audio.Albums.ALBUM_ART))
            }

            cursor!!.close()

            return albumPath
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val view = ItemSongBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SongViewHolder(view)
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        holder.bind(listener ,data[position], position)
    }

    override fun getItemCount(): Int = data.size
}

interface SongListener{
    fun onSongSelected(position: Int, song: ModelAudio)
}