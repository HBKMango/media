package app.android.media.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.android.media.databinding.ItemSingleImageBinding
import com.bumptech.glide.Glide

class AdapterSingleImage : RecyclerView.Adapter<AdapterSingleImage.ImageSingleViewHolder>() {

    private var data: List<String> = ArrayList()

    fun swapData(data: List<String>) {
        this.data = data
        notifyDataSetChanged()
    }

    class ImageSingleViewHolder(private val binding: ItemSingleImageBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: String, position: Int) = with(binding) {
            /*Glide.with(binding.root.context)
                .load(item)
                .into(image)*/
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageSingleViewHolder {
        val view = ItemSingleImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageSingleViewHolder(view)
    }

    override fun getItemCount() : Int = data.size

    override fun onBindViewHolder(holder: ImageSingleViewHolder, position: Int) {
        holder.bind(data[position], position)
    }
}