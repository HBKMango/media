package app.android.media.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import app.android.media.ui.fragment.MusicFragment
import app.android.media.ui.fragment.PicturesFragment
import app.android.media.ui.fragment.ProfileFragment

class PageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return PicturesFragment()
            }
            1 -> {
                return MusicFragment()
            }
            2 -> {
                return ProfileFragment()
            }
        }
        return PicturesFragment()
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        when (position) {
            0 -> {
                return "Pictures"
            }
            1 -> {
                return "Music"
            }
            2 -> {
                return "Profile"
            }
        }
        return "Pictures"
    }
}