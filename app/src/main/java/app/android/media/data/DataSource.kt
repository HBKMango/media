package app.android.media.data

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import app.android.media.model.ModelAudio

object DataSource {

     @SuppressLint("Recycle")
     @RequiresApi(Build.VERSION_CODES.Q)
     fun loadImages(context: Context) : List<String> {

         lateinit var cursor: Cursor
         val list = mutableListOf<String>()

         val uri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
         val projection =
             listOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
         val orderBy = MediaStore.Video.Media.DATE_TAKEN

         cursor = context.contentResolver.query(
             uri,
             projection.toTypedArray(),
             null,
             null,
             "$orderBy DESC"
         )!!

         val column = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)

         while (cursor.moveToNext()) {
             list.add(cursor.getString(column))
         }

         return list
     }

    @RequiresApi(Build.VERSION_CODES.R)
    @SuppressLint("Recycle")
    fun loadMusic(context: Context) : List<ModelAudio>{

        lateinit var cursor: Cursor
        val selection: String? = null
        val projection: Array<String> = arrayOf(
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.ALBUM,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media.SIZE,
            MediaStore.Audio.Media.ALBUM_ID,
        )
        cursor = context.contentResolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
            projection,
            selection,
            null,
            null
        )!!

        val audioList = mutableListOf<ModelAudio>()
        var modelAudio: ModelAudio?

        while (cursor.moveToNext()) {
            modelAudio = ModelAudio(
                "" + cursor.getString(0),
                "" + cursor.getString(1),
                "" + cursor.getString(2),
                "" + cursor.getString(3),
                "" + cursor.getString(4),
                "" + cursor.getString(5),
                "" + cursor.getString(6),
                "" + cursor.getString(7)
            )
            audioList.add(modelAudio)
        }
        return audioList
    }
}